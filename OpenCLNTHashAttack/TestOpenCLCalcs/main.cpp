#include "main.h"
#include "stdio.h"
#include "CL/cl.h"
#include <cstdlib>
#include <time.h>
#include <omp.h>
#include <cstring>
#include <iostream>

main::main(void)
{
}
main::~main(void)
{
}

#define MAX_SOURCE_SIZE (0x100000)
 
void GetCombinationInBaseFormat(unsigned long long numericCombination, const int sizeOfFieldCharacters, char *fieldCharacters, int *sizeOfCombinationInBase, char *combinationInBase)
{	
	char converted_number[25]={0};
	int next_digit, index=0;	
	if(numericCombination==0)
	{
		combinationInBase[0] = fieldCharacters[0];
		*sizeOfCombinationInBase=1;
	}
	else
	{
		while (numericCombination != 0)
		{
			converted_number[index] = fieldCharacters[numericCombination % sizeOfFieldCharacters];
			numericCombination = numericCombination / sizeOfFieldCharacters;
			++index;
		}
		*sizeOfCombinationInBase = index;
		for(int i=index-1;i>=0;i--)
		{
			combinationInBase[(index-1)-i] = converted_number[i];			
		}
	}
}

int main()
{
	#pragma region OpenCL variables
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	#pragma endregion
 
	#pragma region Characters etc
	//int sizeOfFieldCharacters=16;
	//char fieldCharacters[] = {"0123456789ABCDEF"};
	//int sizeOfFieldCharacters=10;
	//char fieldCharacters[] = {"0123456789"};
	//int sizeOfFieldCharacters = 77;
	//char fieldCharacters[] = {"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&.*()_-+="};																							 
	int sizeOfFieldCharacters = 69;
	char fieldCharacters[] = {"eariotnslcudpmhgbfywkEARIOTNSLCUDPMHGBFYWK0123456789~`!@#$%^&*()-=+?."};																							 	
	//int sizeOfFieldCharacters = 62;
	//char fieldCharacters[] = {"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"};

	//char hashToMatch[33] = {"446AD0B9E3BD3563D5256DA23DDFE4DD"}; //112
	//char hashToMatch[33] = {"588630F3BEABC29AF4A695586876082E"}; //333
	char hashToMatch[33] = {"4D8DFDFF9C3721EA86207530A0228C50"}; //33333
	//char hashToMatch[33] = {"DB392F6F4D2F982D5E7C78FA4CC772AB"}; //6934E
	//char hashToMatch[33] = {"B18B9D9B177F0DDA729D7BFE746890EF"}; //BijR
	//char hashToMatch[33] = {"B6B29E059FF0721D90E7298E8F3DE5BB"}; //++++
	//char hashToMatch[33] = {"588630F3BEABC29AF4A695586876082E"}; //333	
	//char hashToMatch[33] = {"B9B124C41E03120699C6EF6E4E032503"}; //la&.h 	
	//char hashToMatch[33] = {"3960237F8663FFD4B69DE27A485B5E22"}; //BAAAA 		
	//char hashToMatch[33] = {"DB5342A7EF477893788896CCCAD89BCA"}; //Doroko09 		
	#pragma endregion

	#pragma region Open file with kernel code
	FILE *fp;
	char fileName[] = "Z:\\SubversionEdgeCode\\Secured\\trunk\\DistributedProcessing\\TestOpenCLCalcs\\sourceOld.cl";
	char *source_str;
	size_t source_size;
 
	/* Load the source code containing the kernel*/
	fopen_s(&fp, fileName, "r");
	if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
	#pragma endregion

	/* Get Platform and Device Info */	
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);

	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
 
	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
 
	#pragma region Device Info
	char deviceName[100];
	ret = clGetDeviceInfo(device_id, CL_DEVICE_NAME,sizeof(deviceName), &deviceName, NULL);
	printf("(CL_DEVICE_NAME): %d\n", deviceName);

	cl_ulong ret_st=0;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE,sizeof(ret_st), &ret_st, NULL);
	printf("(CL_DEVICE_GLOBAL_MEM_SIZE): %d\n", ret_st);

	ret = clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_SIZE,sizeof(ret_st), &ret_st, NULL);
	printf("(CL_DEVICE_LOCAL_MEM_SIZE): %d\n", ret_st);	

	cl_uint maxComputeUnits=0;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS,sizeof(maxComputeUnits), &maxComputeUnits, NULL);
	printf("Max threads (CL_DEVICE_MAX_COMPUTE_UNITS): %d\n", maxComputeUnits);
	
	size_t sizeTValue;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE,sizeof(sizeTValue), &sizeTValue, NULL);
	printf("Max threads (CL_DEVICE_MAX_WORK_GROUP_SIZE): %d\n", sizeTValue);	
	#pragma endregion

	int threads = 4096*32;//maxComputeUnits;
	int blockSize = 1;
	std::cout<<threads<<":"<<blockSize<<"  "<<hashToMatch<<std::endl;
	cl_uint foundMatch = 0;
	cl_ulong *combinationBoundaries = new cl_ulong[threads];
	for(int i=0;i<threads;i++) combinationBoundaries[i]=0;
	
	char matchedCombination[33] = {0};	
	
	/* Create Memory Buffer */
	cl_mem memobjFieldCharacters = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(fieldCharacters), NULL, &ret);
	cl_mem memobjHashToMatch = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(hashToMatch), NULL, &ret);
	cl_mem memobjCombinationBoundaries = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(cl_ulong) * threads, NULL, &ret);
	cl_mem memobjMatchedCombination = clCreateBuffer(context, CL_MEM_WRITE_ONLY,sizeof(matchedCombination), NULL, &ret);
	cl_mem memobjFoundMatch = clCreateBuffer(context, CL_MEM_READ_WRITE,sizeof(foundMatch), NULL, &ret);
 
	/* Create Kernel Program from the source */
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
	(const size_t *)&source_size, &ret);
 
	/* Build Kernel Program */
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);


	/* Create OpenCL Kernel */
	kernel = clCreateKernel(program, "Calculate", &ret);
 
	ret = clEnqueueWriteBuffer(command_queue, memobjFieldCharacters, CL_TRUE, 0, sizeof(char) * sizeOfFieldCharacters, fieldCharacters, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, memobjHashToMatch, CL_TRUE, 0, sizeof(char) * 33, hashToMatch, 0, NULL, NULL);
	/* Set OpenCL Kernel Parameters */	
	ret = clSetKernelArg(kernel, 0, sizeof(int), &blockSize);
	ret = clSetKernelArg(kernel, 1, sizeof(int), &sizeOfFieldCharacters);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&memobjFieldCharacters);	
	ret = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&memobjHashToMatch);	
	ret = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&memobjMatchedCombination);	
	ret = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&memobjCombinationBoundaries);
	ret = clSetKernelArg(kernel, 6, sizeof(cl_mem), (void *)&memobjFoundMatch);	

	
	int timeReportloop = 0;
	time_t startTime = time(0);
	for(int loop=0;loop<=INT_MAX;loop=loop+(threads * blockSize))
	{		
		timeReportloop++;
		for(int l=0;l<threads;l++)
		{
			combinationBoundaries[l] = loop+(l*blockSize);
		}

		ret = clEnqueueWriteBuffer(command_queue, memobjCombinationBoundaries, CL_TRUE, 0, sizeof(cl_ulong) * threads, combinationBoundaries, 0, NULL, NULL);
		

		size_t a = threads;
		size_t b = blockSize;
		ret = clEnqueueNDRangeKernel(command_queue, kernel, 1,NULL,&a,NULL, 0, NULL,NULL);

		ret = clEnqueueReadBuffer(command_queue, memobjFoundMatch, CL_TRUE, 0, sizeof(foundMatch), &foundMatch, 0, NULL, NULL);				
		time_t now = time(0);
		double diff = difftime(now, startTime);
		if(foundMatch==1)	
		{
			ret = clEnqueueReadBuffer(command_queue, memobjMatchedCombination, CL_TRUE, 0, sizeof(matchedCombination), matchedCombination, 0, NULL, NULL);			
			
			int days = floor(diff/86400);//1
			int hours = floor((diff-(days*86400))/3600);//4
			int minutes = floor((diff - (days*86400) - (hours*3600))/60);//3
			int seconds = floor((diff - (days*86400) - (hours*3600)) - (minutes*60));//30
			std::cout<<"days: "<<days<<" hour: "<<hours<<" min: "<<minutes<<" sec:"<<seconds;
			break;
		}
		if(timeReportloop % 1500==0)
		{
			int sizeOfCombinationInBase = 33;
			char combinationInBase[33]={0};			
			GetCombinationInBaseFormat(combinationBoundaries[threads-1], sizeOfFieldCharacters, fieldCharacters, &sizeOfCombinationInBase, combinationInBase);
			timeReportloop = 0;
			int days = floor(diff/86400);//1
			int hours = floor((diff-(days*86400))/3600);//4
			int minutes = floor((diff - (days*86400) - (hours*3600))/60);//3
			int seconds = floor((diff - (days*86400) - (hours*3600)) - (minutes*60));//30
			std::cout<<days<<"-"<<hours<<":"<<minutes<<":"<<seconds<<"(D-h:m:s) : "<<combinationBoundaries[threads-1]/diff<<" c/s"<<" - "<<combinationInBase<<std::endl;
		}
	}
	
	/* Finalization */
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(memobjFieldCharacters);
	ret = clReleaseMemObject(memobjHashToMatch);
	ret = clReleaseMemObject(memobjCombinationBoundaries);
	ret = clReleaseMemObject(memobjMatchedCombination);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
 
	delete(combinationBoundaries);
	free(source_str);

 
	return 0;
}


int mainNew()
{
	#pragma region OpenCL variables
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue command_queue = NULL;
	cl_program program = NULL;
	cl_kernel kernel = NULL;
	cl_platform_id platform_id = NULL;
	cl_uint ret_num_devices;
	cl_uint ret_num_platforms;
	cl_int ret;
	#pragma endregion
 
	#pragma region Characters etc
	//int sizeOfFieldCharacters=16;
	//char fieldCharacters[] = {"0123456789ABCDEF"};
	//int sizeOfFieldCharacters=10;
	//char fieldCharacters[] = {"0123456789"};																						 
	int sizeOfFieldCharacters = 62;
	char fieldCharacters[] = {"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"};																							 	

	//char hashToMatch[33] = {"446AD0B9E3BD3563D5256DA23DDFE4DD"}; //112
	//char hashToMatch[33] = {"A0950B8E26E43D021489BB2D9F8BD55A"}; //333333
	char hashToMatch[33] = {"4D8DFDFF9C3721EA86207530A0228C50"}; //33333 		
	#pragma endregion

	#pragma region Open file with kernel code
	FILE *fp;
	char fileName[] = "Z:\\SubversionEdgeCode\\Secured\\trunk\\DistributedProcessing\\TestOpenCLCalcs\\source.cl";
	char *source_str;
	size_t source_size;
 
	/* Load the source code containing the kernel*/
	fopen_s(&fp, fileName, "r");
	if (!fp) {
	fprintf(stderr, "Failed to load kernel.\n");
	exit(1);
	}
	source_str = (char*)malloc(MAX_SOURCE_SIZE);
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
	fclose(fp);
	#pragma endregion

	/* Get Platform and Device Info */	
	ret = clGetPlatformIDs(1, &platform_id, &ret_num_platforms);

	ret = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices);

	/* Create OpenCL context */
	context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
 
	/* Create Command Queue */
	command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
 
	#pragma region Device Info
	char deviceName[100];
	ret = clGetDeviceInfo(device_id, CL_DEVICE_NAME,sizeof(deviceName), &deviceName, NULL);
	printf("(CL_DEVICE_NAME): %d\n", deviceName);

	cl_ulong ret_st=0;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE,sizeof(ret_st), &ret_st, NULL);
	printf("(CL_DEVICE_GLOBAL_MEM_SIZE): %d\n", ret_st);

	ret = clGetDeviceInfo(device_id, CL_DEVICE_LOCAL_MEM_SIZE,sizeof(ret_st), &ret_st, NULL);
	printf("(CL_DEVICE_LOCAL_MEM_SIZE): %d\n", ret_st);	

	cl_uint maxComputeUnits=0;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS,sizeof(maxComputeUnits), &maxComputeUnits, NULL);
	printf("Max threads (CL_DEVICE_MAX_COMPUTE_UNITS): %d\n", maxComputeUnits);
	
	size_t sizeTValue;
	ret = clGetDeviceInfo(device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE,sizeof(sizeTValue), &sizeTValue, NULL);
	printf("Max threads (CL_DEVICE_MAX_WORK_GROUP_SIZE): %d\n", sizeTValue);	
	#pragma endregion

	int threads = 512;//maxComputeUnits;
	std::cout<<threads<<hashToMatch<<std::endl;
	char *hashes = new char[threads*33];
	int matched = 0;
	for(int i=0;i<threads*33;i++)
	{
		hashes[i]=0;
	}
	
	/* Create Memory Buffer */	
	cl_mem memobjHashToMatch = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(hashToMatch), NULL, &ret);
	cl_mem memobjHashes = clCreateBuffer(context, CL_MEM_READ_ONLY,sizeof(char) * threads * 33, NULL, &ret);
	cl_mem memobjMatched = clCreateBuffer(context, CL_MEM_READ_WRITE,sizeof(int), NULL, &ret);
	 
	/* Create Kernel Program from the source */
	program = clCreateProgramWithSource(context, 1, (const char **)&source_str,
	(const size_t *)&source_size, &ret);
	ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	kernel = clCreateKernel(program, "CalculateNew", &ret);
	
	ret = clEnqueueWriteBuffer(command_queue, memobjHashToMatch, CL_TRUE, 0, sizeof(char) * 33, hashToMatch, 0, NULL, NULL);
	ret = clEnqueueWriteBuffer(command_queue, memobjMatched, CL_TRUE, 0, sizeof(int), &matched, 0, NULL, NULL);
	ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&memobjHashToMatch);				
	ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&memobjHashes);
	ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&memobjMatched);


	int sizeOfCombinationInBase = 33;
	char combinationInBase[33]={0};	
	int timeReportloop = 0;
	time_t startTime = time(0);

	int local = 0; 
	int remote = 0;

	startTime = time(0);
	for(int loop=0;loop<=INT_MAX;loop+=threads)
	{				
		timeReportloop++;				
		
		omp_set_num_threads(omp_get_max_threads());
		#pragma omp parallel
		{
			int threadNum = omp_get_thread_num();
			int threadsRunning = omp_get_max_threads();
			for(int inner=0;inner<threads/threadsRunning;inner++)
			{
				int index = (inner + (threadNum*(threads/threadsRunning)))*33;
				GetCombinationInBaseFormat(inner + (threadNum * (threads/threadsRunning)) + loop, sizeOfFieldCharacters, fieldCharacters, &sizeOfCombinationInBase, combinationInBase);
				memcpy(&hashes[index], combinationInBase, (size_t) 33);				
			}
		}

		ret = clEnqueueWriteBuffer(command_queue, memobjHashes, CL_TRUE, 0, sizeof(char) * threads * 33, hashes, 0, NULL, NULL);		

		size_t a = threads;
		ret = clEnqueueNDRangeKernel(command_queue, kernel, 1,NULL,&a,NULL, 0, NULL,NULL);
		ret = clEnqueueReadBuffer(command_queue, memobjMatched, CL_TRUE, 0, sizeof(int), &matched, 0, NULL, NULL);		
		if(matched==1)
		{
			matched=22;
		}
	

		if(timeReportloop % 5000==0)
		{			
			time_t now = time(0);
			double diff = difftime(now, startTime);
			timeReportloop = 0;
			int days = floor(diff/86400);//1
			int hours = floor((diff-(days*86400))/3600);//4
			int minutes = floor((diff - (days*86400) - (hours*3600))/60);//3
			int seconds = floor((diff - (days*86400) - (hours*3600)) - (minutes*60));//30
			std::cout<<days<<"-"<<hours<<":"<<minutes<<":"<<seconds<<"(D-h:m:s) : "<<combinationInBase<<":"<<loop/diff<<std::endl;
		}
	}
	
	/* Finalization */
	ret = clFlush(command_queue);
	ret = clFinish(command_queue);
	ret = clReleaseKernel(kernel);
	ret = clReleaseProgram(program);
	ret = clReleaseMemObject(memobjHashToMatch);
	ret = clReleaseCommandQueue(command_queue);
	ret = clReleaseContext(context);
 
	delete(hashes);
	free(source_str);
 
	return 0;
}