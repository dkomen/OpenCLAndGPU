# OpenCL and GPU processing
An example of how the multiple 'cores' of a GPU can be used to do work.
Having a windows password hash (easily retrieved from Windows using Linux), try a bruteforce attack using the GPU of the video card.<br />
Note: OpenCL must be installed on PC.<br />
Visual Studio 2019, OpenCL, C++, C
